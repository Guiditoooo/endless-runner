#include "scenemaster.h"
#include <iostream>

namespace endless {

	namespace config {

		namespace scenes {

			void sceneManager(Scene& scene, Scene nextScene) {

				if (scene != nextScene) {

					switch (scene) //deinit
					{
					case Scene::MENU:
						menu::deinit();
						//std::cout << "MENU Deinited";
						break;
					case Scene::GAME:
						game::deinit();
						//std::cout << "GAME Deinited";
						break;
					case Scene::CREDITS:
						credits::deinit();
						//std::cout << "GAMEs Inited";
						break;
					}

					


					switch (nextScene)//init
					{
					case Scene::MENU:
						menu::init();
						//std::cout << "MENU Inited";
						break;
					case Scene::GAME:
						game::init();
						//std::cout << "GAMEs Inited";
						break;
					case Scene::CREDITS:
						credits::init();
						//std::cout << "GAMEs Inited";
						break;
					}

					scene = nextScene;

				}

				else
				{
					//std::cout << "On Scene > ";
					switch (scene) //deinit
					{
					case Scene::MENU:
						//std::cout << "MENU\n";
						break;
					case Scene::GAME:
						//std::cout << "GAME\n";
						break;
					}
				}

			}

		}

		void generalInit() {

			InitWindow(screen::width, screen::height, "Endless Ghost Adventure");
			SetTargetFPS(screen::fpsRate);
			InitAudioDevice();

		}

		void generalDeinit() {
			CloseAudioDevice();
		}

	}


	void runGame() {

		config::generalInit();

		do {

			using namespace config;
			using namespace scenes;

			if (IsKeyPressed(KEY_UP) || IsKeyPressed(KEY_DOWN)) {
				std::cout << "player esta en  " << game::player.obj.hitbox.y << "\n";
			}

			scenes::sceneManager(scene, next_scene);

			switch (scene) {

			case Scene::MENU:
				menu::update();
				menu::draw();
				break;
			case Scene::GAME:
				game::update();
				game::draw();
				break;
			case Scene::CREDITS:
				credits::update();
				credits::draw();
				break;
			case Scene::QUIT:
				menu::continueInGame = false;
				break;

			}

		} while (!WindowShouldClose() && menu::continueInGame);

		config::generalDeinit();

	}



}