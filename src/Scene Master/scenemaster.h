#ifndef MENU_SCENEMASTER
#define MENU_SCENEMASTER
#include "Extern Vars/extern.h"
#include "Scenes/Game/game.h"
#include "Scenes/Menu/menu.h"
#include "Scenes/Credits/credits.h"

namespace endless {

	namespace config {

		namespace scenes {

			void sceneManager(Scene&, Scene);

		}

		void generalInit();
		void generalDeinit();

	}

	void runGame();

}
#endif