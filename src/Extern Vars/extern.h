#ifndef MENU_EXTERN
#define MENU_EXTERN

#include "raylib.h"
#include <iostream>

namespace endless {

	const float plusSelected = 1.15f;

	struct Text {

		std::string tx;
		Vector2 pos = { 0,0 };
		int size = 5;
		Color color = WHITE;

	};

	enum class menuOptions { PLAY = 0, OPTIONS, CREDITS, EXIT, MENU, RESUME, RESOLUTION, NONE };

	struct Button {

		Color color = BLACK;
		Rectangle square;
		menuOptions id;
		bool selected = false;
		Text txt;

	};

	namespace config {

		namespace screen{
		
			extern int width;
			extern int height;
			extern int fpsRate;
		
		}

		namespace scenes {

			enum class Scene {
				MENU,
				GAME,
				CREDITS,
				QUIT,
				NONE
			};

			extern Scene scene;
			extern Scene next_scene;

		}

		Color getRandomColor(int min, int max);

	}

	namespace game {

		const int LVL_COUNT = 3;

		enum class OrderY {
			TOP,
			MID,
			BOT,
			ALL
		};

		enum class OrderX {
			FST,
			SND,
			TRD
		};

		enum class EnemyType {
			GIANT,
			WEAK,
			NORMAL,
			NONE
		};

		struct Texture {
			Texture2D TX;
			Color color = WHITE;
			Rectangle hitbox;
			OrderY ordY;
			OrderX ordX;
		};

		struct GameObject {
			Texture obj;
			OrderY ordY;
			EnemyType type;
			int HP;
			bool visible = false;
			bool active = false;
			bool jumping = false;
			bool destroyable = true;
		};


		extern float jumpOrigin[LVL_COUNT];
		extern float jumpDestin[LVL_COUNT];

		extern int enemyKillCount;

	}

	void drawButton(Button btn);

}
#endif