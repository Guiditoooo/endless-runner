#include "extern.h"

namespace endless {

	void drawButton(Button btn) {
		DrawRectangleRec(btn.square, btn.color);
		DrawRectangleLinesEx(btn.square, 3, btn.txt.color);
		DrawText(&btn.txt.tx[0], btn.txt.pos.x, btn.txt.pos.y, btn.txt.size, btn.txt.color);
	}

	namespace config {

		namespace screen {

			int width = 1000;
			int height = 800;
			int fpsRate = 60;

		}

		namespace scenes {

			Scene scene = Scene::NONE;
			Scene next_scene = Scene::MENU;

		}

		Color getRandomColor(int min, int max) {

			return { static_cast<unsigned char>(GetRandomValue(min, max)), static_cast<unsigned char>(GetRandomValue(min, max)), static_cast<unsigned char>(GetRandomValue(min,max)), static_cast<unsigned char>(GetRandomValue(200,255)) };

		}

	}

}