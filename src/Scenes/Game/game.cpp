#include "game.h"
#include <iostream>
#include <string>

namespace endless {

	namespace game {

		Texture BGF[LVL_COUNT][BG_COUNT]; //ADELANTE
		Texture BGL[LVL_COUNT][BG_COUNT]; //ATRAS
		Texture cloud[LVL_COUNT];
		Texture sun[LVL_COUNT];
		Texture window[LVL_COUNT];
		Texture floor[LVL_COUNT];
		Texture enemies[HowManyEnemyTypes];

		Music BGM;
		Texture2D BGFront;
		Texture2D BGBack;
		Texture2D cloudTX;
		Texture2D floorTX;
		Texture2D windowTX;
		Texture2D sunTX;

		Sound shot;
		Sound buttonBeep;

		GameObject player;
		GameObject missile;
		GameObject enemy[HowManyEnemies];

		Rectangle begin;
		Rectangle end;

		float jumpOrigin[LVL_COUNT];
		float jumpDestin[LVL_COUNT];

		int cloudVel[LVL_COUNT];
		int BGFVel[LVL_COUNT];
		int BGLVel[LVL_COUNT];
		int enemyVel[HowManyEnemies];

		int enemyKillCount;

		bool pause;
		bool gameOver;
		bool showingInstructions = true;

		bool pauseMusic;
		bool pauseSounds;
		bool spawnEnemies=true;

		bool goingUp=false;
		bool goingDown=false;

		bool pauseCreation = true;
		bool resolCreation;
		
		int auxKeyPressed = 10000;

		int enemy_count = 0;
		float frames_expend = 0;
		int time_in_seconds = 0;

		void init() {

			LoadMusics();
			pauseSounds = false;
			pauseMusic = false;

			pause = false;
			gameOver = false;
			resolCreation = true;
			pauseCreation = true;

			if (pauseCreation) {
			
				using namespace pauseMenu;

				#pragma region PAUSE AREA

				pRec.height = config::screen::height / 2;
				pRec.width = config::screen::width / 2;
				pRec.x = config::screen::width / 2 - pRec.width / 2;
				pRec.y = config::screen::height / 2 - pRec.height / 2;

				pText.tx = "PAUSED";
				pText.size = config::screen::height / 11;
				pText.pos.x = config::screen::width / 2 - MeasureText(&pText.tx[0], pText.size) / 2;
				pText.pos.y = pRec.y + pText.size * 5 / 7;
				pText.color = BLACK;

				#pragma region RESUME

				resume.txt.tx = "RESUME";
				resume.id = menuOptions::RESUME;
				resume.txt.size = config::screen::height * 2 / 39;
				resume.square.width = MeasureText(&resume.txt.tx[0], resume.txt.size) * 3 / 2;
				resume.square.height = resume.txt.size * 2;
				resume.square.x = pRec.x + pRec.width / 2 - resume.square.width / 2;
				resume.square.y = pText.pos.y + pText.size * 3 / 2;
				resume.color = pauseButtonsColor;
				resume.txt.pos.x = resume.square.x + resume.square.width / 2 - MeasureText(&resume.txt.tx[0], resume.txt.size) / 2;
				resume.txt.pos.y = resume.square.y + resume.square.height / 2 - resume.txt.size / 2;
				resume.txt.color = BLACK;

#pragma endregion

				#pragma region MAIN MENU

				mainMenu.txt.tx = "MAIN MENU";
				mainMenu.id = menuOptions::MENU;
				mainMenu.txt.size = config::screen::height * 2 / 39;
				mainMenu.square.width = MeasureText(&mainMenu.txt.tx[0], mainMenu.txt.size) * 3 / 2;
				mainMenu.square.height = mainMenu.txt.size * 2;
				mainMenu.square.x = pRec.x + pRec.width / 2 - mainMenu.square.width / 2;
				mainMenu.square.y = resume.square.y + pText.size * 3 / 2;
				mainMenu.color = pauseButtonsColor;
				mainMenu.txt.pos.x = mainMenu.square.x + mainMenu.square.width / 2 - MeasureText(&mainMenu.txt.tx[0], mainMenu.txt.size) / 2;
				mainMenu.txt.pos.y = mainMenu.square.y + mainMenu.square.height / 2 - mainMenu.txt.size / 2;
				mainMenu.txt.color = BLACK;

				pauseButtons[0] = resume;
				pauseButtons[1] = mainMenu;

#pragma	endregion

#pragma endregion

			
			}

			#pragma region BACKGROUNDS

			std::cout << std::endl;
			BGFront = LoadTexture("res/assets/Background/BG11.png");
			std::cout << std::endl;
			BGBack = LoadTexture("res/assets/Background/BG10.png");
			std::cout << std::endl;
			cloudTX = LoadTexture("res/assets/Background/cloud.png");
			std::cout << std::endl;
			floorTX = LoadTexture("res/assets/Background/carpet.png");
			std::cout << std::endl;
			windowTX = LoadTexture("res/assets/Background/window.png");
			std::cout << std::endl;
			sunTX = LoadTexture("res/assets/Background/sun.png");
			std::cout << std::endl;

			for (short i = 0; i < LVL_COUNT; i++) {

				floor[i].TX = floorTX;
				floor[i].TX.height /= 2;
				floor[i].TX.width = config::screen::width;
				floor[i].hitbox.y = config::screen::height * (i + 1) / 3 - floor[i].TX.height;

				window[i].TX = windowTX;
				window[i].TX.height = config::screen::height / 3 - floor[i].TX.height + 5;
				window[i].TX.width = config::screen::width;
				window[i].hitbox.y = config::screen::height * i / 3;

				for (short j = 0; j < BG_COUNT; j++) {

					BGF[i][j].TX = BGFront;
					BGF[i][j].TX.height = config::screen::height / LVL_COUNT * 3 / 4;
					BGF[i][j].TX.width = config::screen::width * 3 / 4;
					BGF[i][j].hitbox.y = config::screen::height * i / 3;
					BGF[i][j].hitbox.x = BGF[i][j].TX.width * j;
					BGF[i][j].hitbox.y = BGF[i][j].hitbox.y;
					BGF[i][j].hitbox.width = 10;
					BGF[i][j].hitbox.height = BGF[i][j].TX.height;
					BGF[i][j].ordY = static_cast<OrderY>(i);
					BGF[i][j].ordX = static_cast<OrderX>(j);

					BGL[i][j].TX = BGBack;
					BGL[i][j].TX.height = config::screen::height / LVL_COUNT * 3 / 4;
					BGL[i][j].TX.width = config::screen::width * 2 / 3;
					BGL[i][j].hitbox.y = config::screen::height * i / 3;
					BGL[i][j].hitbox.x = BGL[i][j].TX.width * j;
					BGL[i][j].hitbox.y = BGL[i][j].hitbox.y;
					BGL[i][j].hitbox.width = 10;
					BGL[i][j].hitbox.height = BGL[i][j].TX.height;
					BGL[i][j].ordY = static_cast<OrderY>(i);
					BGL[i][j].ordX = static_cast<OrderX>(j);

				}

				BGFVel[i] = GetRandomValue(45, 75);
				BGLVel[i] = GetRandomValue(10, 35);

				cloud[i].TX = cloudTX;
				cloud[i].ordY = static_cast<OrderY>(i);
				ResetCloud(cloud[i]);
				cloudVel[i] = GetRandomValue(60, 180);
				cloud[i].hitbox.x = cloud[i].hitbox.x;
				cloud[i].hitbox.y = cloud[i].hitbox.y;
				cloud[i].hitbox.width = cloud[i].TX.width;
				cloud[i].hitbox.height = cloud[i].TX.height;

				sun[i].TX = sunTX;
				sun[i].TX.height = (config::screen::height / 3 - floor[i].TX.height) * 3 / 4;
				sun[i].TX.width = (config::screen::height / 3 - floor[i].TX.height) * 3 / 4;
				sun[i].hitbox.y = config::screen::height * i / 3 - sun[i].TX.height * 1 / 4;
				sun[i].hitbox.x = config::screen::width - sun[i].TX.width * 6 / 7;

			}


			#pragma endregion

			#pragma region PLAYER

			player.obj.TX = LoadTexture("res/assets/player/idle.png");
			std::cout << std::endl;
			player.obj.TX.width = config::screen::width / 14;
			player.obj.TX.height = config::screen::height / 10;
			player.obj.hitbox.x = config::screen::width * 2 / 20;
			player.obj.hitbox.y = config::screen::height * 2 / 3 - player.obj.TX.height * 13 /10;
			player.obj.hitbox.width = player.obj.TX.width;
			player.obj.hitbox.height = player.obj.TX.height;
			player.ordY = OrderY::MID;

			for (short i = 0; i < LVL_COUNT; i++) {

				jumpOrigin[i] = config::screen::height * (i+1)/ 3 - player.obj.TX.height * 13 / 10;
				jumpDestin[i] = config::screen::height * i / 3;

			}
			

			#pragma endregion

			#pragma region MISILE

			missile.obj.TX = LoadTexture("res/assets/player/missile.png");
			std::cout << std::endl;
			missile.obj.TX.width = player.obj.TX.height / 2;
			missile.obj.TX.height = player.obj.TX.height / 2;
			missile.obj.hitbox = { static_cast<float>(missile.obj.hitbox.x), static_cast<float>(missile.obj.hitbox.y), static_cast<float>(missile.obj.TX.width), static_cast<float>(missile.obj.TX.height) };
			ResetMissile();

			#pragma endregion

			#pragma region LIMITS

			begin.x = - BGL[0][0].TX.width - 10;
			begin.width = 10;
			begin.y = 0;
			begin.height = config::screen::height;

			end.x = BGL[0][0].TX.width * 2;
			end.width = 10;
			end.y = 0;
			end.height = config::screen::height;

			#pragma endregion

			#pragma region ENEMIES

			enemyKillCount = 0;
			enemy_count = 0;

			enemies[0].TX = LoadTexture("res/assets/enemy/ene0.png");
			std::cout << std::endl;
			enemies[0].TX.height = config::screen::height / 3;
			enemies[0].TX.width = config::screen::width * 2 / 4;
			enemies[0].hitbox.x = config::screen::width + enemies[0].TX.width;
			enemies[0].hitbox.y = - config::screen::height * 1 / 7;
			enemies[0].ordY = OrderY::ALL;

			enemies[1].TX = LoadTexture("res/assets/enemy/ene1.png");
			std::cout << std::endl;
			enemies[1].TX.height = missile.obj.TX.height * 3 / 2;
			enemies[1].TX.width = missile.obj.TX.width * 3 / 2;
			enemies[1].hitbox.x = config::screen::width + enemies[1].TX.width;

			enemies[2].TX = LoadTexture("res/assets/enemy/ene2.png");
			std::cout << std::endl;
			enemies[2].TX.height = player.obj.TX.height * 2;
			enemies[2].TX.width = player.obj.TX.width * 2;
			enemies[2].hitbox.x = config::screen::width + enemies[2].TX.width;

			for (short i = 0; i < HowManyEnemies; i++) {

				enemy[i].active = false;
				enemy[i].destroyable = false;
				enemy[i].obj.hitbox = {0,0,0,0};
				enemy[i].visible = false;
				enemy[i].type = EnemyType::NONE;
				enemy[i].ordY = OrderY::ALL;

			}

			#pragma endregion

		}

		void update() {

			if (!gameOver && !showingInstructions) {

				frames_expend += GetFrameTime();

				float gravity = GetFrameTime() * 130.0f;
				float speed = 0.0f;

				if (IsKeyPressed(KEY_P)) {
					pause = !pause;
				}
				if (IsKeyPressed(KEY_I)) {
					showingInstructions = true;
				}
				if (IsKeyPressed(KEY_G)) {
					gameOver = true;
				}
				if (IsKeyPressed(KEY_M)) {
					pauseMusic = !pauseMusic;
				}
				if (IsKeyPressed(KEY_N)) {
					pauseSounds = !pauseSounds;
				}
				if (IsKeyPressed(KEY_Q)) {
					spawnEnemies = !spawnEnemies;
				}

				if (!pause) {

					float frame_time = GetFrameTime();

					#pragma region ENEMY CREATION

					if (static_cast<int>(frames_expend) % 60 >= 2 || IsKeyPressed(KEY_O)) {

						frames_expend = 0;

						bool created_one = false;

						//----------- ENEMY CREATION
						if (spawnEnemies) {

							for (short i = 0; i < HowManyEnemies; i++) {

								if (!created_one && !enemy[i].active && enemy_count < HowManyEnemies) {

									CreateEnemy(enemy[i]);
									enemy_count++;
									enemyVel[i] = GetRandomValue(110, 190);
									std::cout << "Enemigo Nro " << i << " creado con exito!\n";
									std::cout << "Su velocidad es: " << enemyVel[i] << "\n";
									std::cout << "Su posicion es: (" << enemy[i].obj.hitbox.x << "," << enemy[i].obj.hitbox.y << ")\n";
									std::cout << "Su dimension es: (" << enemy[i].obj.hitbox.width << "," << enemy[i].obj.hitbox.height << ")\n";
									created_one = true;

								}

							}

							std::cout << " Enemigos  " << enemy_count << " / " << HowManyEnemies << "\n";

						}

					}

#pragma endregion

					if (!pauseMusic) {
						UpdateMusicStream(BGM);
					}

					#pragma region BACKGROUND MECH

					for (short i = 0; i < LVL_COUNT; i++) {

						cloud[i].hitbox.x -= frame_time * cloudVel[i];

						if (CheckCollisionRecs(cloud[i].hitbox, begin)) {
							ResetCloud(cloud[i]);
						}

						//---------- PARALLAX
						for (short j = 0; j < BG_COUNT; j++) {

							BGL[i][j].hitbox.x -= frame_time * BGLVel[i];

							BGF[i][j].hitbox.x -= frame_time * BGFVel[i];

							if (CheckCollisionRecs(BGL[i][j].hitbox, begin)) {

								ResetBackground(BGL[i][j]);
								std::cout << "BGL PISO " << i << " COLUMNA " << j << " reseted\n";

							}

							if (CheckCollisionRecs(BGF[i][j].hitbox, begin)) {

								ResetBackground(BGF[i][j]);
								std::cout << "BGF PISO " << i << " COLUMNA " << j << " reseted\n";

							}

						}

					}

#pragma endregion

					//-------------- ENEMY MOVE
					for (short i = 0; i < HowManyEnemies; i++) {

						if (enemy[i].active) {

							enemy[i].obj.hitbox.x -= frame_time * enemyVel[i];

							if (CheckCollisionRecs(enemy[i].obj.hitbox, begin)) {

								enemy[i].active = false;
								enemy_count--;

							}
							//std::cout << "El enemigo " << i << " esta en la x: " << enemy[i].obj.hitbox.x << "\n";
						}

					}

					//-------------- MOVE UP
					if (IsKeyPressed(KEY_UP) && player.ordY != OrderY::TOP && !player.jumping) {

						player.obj.hitbox.y -= config::screen::height / 3;
						player.obj.hitbox = player.obj.hitbox;
						player.ordY = static_cast<OrderY>(static_cast<int>(player.ordY) - 1);
						std::cout << "player sube a " << player.obj.hitbox.y << "\n";

					}

					//-------------- MOVE DOWN
					if (IsKeyPressed(KEY_DOWN) && player.ordY != OrderY::BOT && !player.jumping) {

						player.obj.hitbox.y += config::screen::height / 3;
						player.ordY = static_cast<OrderY>(static_cast<int>(player.ordY) + 1);
						std::cout << "player bajo a " << player.obj.hitbox.y << "\n";
					}

					bool top = (player.obj.hitbox.y < jumpOrigin[static_cast<int>(OrderY::TOP)] + 1.9f && player.obj.hitbox.y > jumpOrigin[static_cast<int>(OrderY::TOP)] - 1.9f);
					bool mid = (player.obj.hitbox.y < jumpOrigin[static_cast<int>(OrderY::MID)] + 1.9f && player.obj.hitbox.y > jumpOrigin[static_cast<int>(OrderY::MID)] - 1.9f);
					bool bot = (player.obj.hitbox.y < jumpOrigin[static_cast<int>(OrderY::BOT)] + 1.9f && player.obj.hitbox.y > jumpOrigin[static_cast<int>(OrderY::BOT)] - 1.9f);

					#pragma region JUMPING

					if (IsKeyPressed(KEY_S) && (top || mid || bot)) {

						std::cout << "Estoy queriendo saltar\n";
						player.jumping = true;

					}

					if (player.jumping) {

						if (player.obj.hitbox.y > jumpDestin[static_cast<int>(player.ordY)] && !goingDown) {

							goingUp = true;

						}
						else {

							goingDown = true;
							goingUp = false;

						}
						if (goingUp) {
							speed -= gravity;
							player.obj.hitbox.y += speed;
							top = false;
							mid = false;
							bot = false;

						}
						if (goingDown) {

							speed += gravity;
							player.obj.hitbox.y += speed;

						}

						switch (player.ordY) {

						case endless::game::OrderY::TOP:

							if (top) {
								std::cout << "Estoy en el piso de TOP\n";
								player.obj.hitbox.y = config::screen::height / 3 - player.obj.TX.height * 13 / 10;
							}
							break;

						case endless::game::OrderY::MID:

							if (mid) {
								std::cout << "Estoy en el piso de MID\n";
								player.obj.hitbox.y = config::screen::height * 2 / 3 - player.obj.TX.height * 13 / 10;
							}
							break;

						case endless::game::OrderY::BOT:

							if (bot) {
								std::cout << "Estoy en el piso de BOT\n";
								player.obj.hitbox.y = config::screen::height - player.obj.TX.height * 13 / 10;
							}
							break;
						}

						if (top || mid || bot) {
							player.jumping = false;
							goingUp = false;
							goingDown = false;
							std::cout << "deje de saltar\n";
						}

					}

#pragma endregion

					#pragma region SHOOT MISSILE

					if (IsKeyPressed(KEY_A) && !missile.active) {
						missile.active = true;
						if (!pauseSounds)
							PlaySound(shot);
						std::cout << "FIUM!\n";
						ResetMissile();
					}

					if (missile.active) {
						ShootMissile();
					}

#pragma endregion	

					#pragma region ENEMY COLLIDER

					for (short i = 0; i < enemy_count; i++) {
						if (enemy[i].active) {
							if (enemy[i].destroyable && missile.active) {
								if (CheckCollisionRecs(missile.obj.hitbox, enemy[i].obj.hitbox)) {
									enemy[i].active = false;
									enemy[i].visible = false;
									std::cout << "El enemigo " << i << " hizo pum!\n";
									missile.active = false;
									missile.visible = false;
									enemy_count--;
									enemyKillCount++;
									std::cout << "Enemigos matados hasta ahora: " << enemyKillCount <<"\n";
								}
							}
						}
						if (CheckCollisionRecs(enemy[i].obj.hitbox, player.obj.hitbox)) {
							gameOver = true;
							resolCreation = false;
						}
					}

#pragma endregion					

				}
				else {
					PauseMenuCheck();
				}

			}
			else { //SI YA PERDISTE
				if (!resolCreation) {
					CreateResolutionWindow();
					resolCreation = true;
				}

				BackToMenuCheck();
			}

			if (showingInstructions && (IsKeyPressed(KEY_P))) {
				showingInstructions = false;
			}

		}
	
		void draw() {

			BeginDrawing();
			if (!gameOver) {
				if (!showingInstructions) {
					ClearBackground(SKYBLUE);

					for (short i = 0; i < LVL_COUNT; i++) {

						for (short j = 0; j < BG_COUNT; j++)
						{
							DrawTX(BGL[i][j]);
						}

						for (short j = 0; j < BG_COUNT; j++)
						{
							DrawTX(BGF[i][j]);
						}

						DrawTX(sun[i]);

					}

					for (short i = 0; i < LVL_COUNT; i++) {

						DrawTX(cloud[i]);
						DrawTX(window[i]);
						DrawTX(floor[i]);

					}

					DrawTX(player.obj);
#if _DEBUG
					DebugLines(player.obj, WHITE);
#endif

					for (short i = 0; i < HowManyEnemies; i++) {
						if (enemy[i].active) {
							DrawTX(enemy[i].obj);
#if _DEBUG
							DebugLines(enemy[i].obj, LIME);
#endif
						}
					}

					if (missile.visible) {
						DrawTX(missile.obj);
#if _DEBUG
						DebugLines(missile.obj, RED);
#endif
					}

					DrawTX(enemies[0]);

					if (pause) {
						using namespace pauseMenu;

						DrawRectangleRec(pRec, pauseBG);
						DrawRectangleLinesEx(pRec, 2, BLACK);
						DrawText(&pText.tx[0], pText.pos.x, pText.pos.y, pText.size, pText.color);
						for (short i = 0; i < howManyPauseButtons; i++) {
							if (pauseButtons[i].selected) {
								pauseButtons[i].color = LIME;
							}
							else {
								pauseButtons[i].color = WHITE;
							}
							drawButton(pauseButtons[i]);
						}
					}
				}
				else {
					ClearBackground(DARKGRAY);

					std::string tx = "INSTRUCTIONS!";

					std::string a = "Use the arrow up and down to move betheen rails.";
					std::string b = "Press A to shoot a missile.";
					std::string c = "Press S to Jump (to avoid small enemies).";
					std::string d = "Press P to pause. Use it to start the game.";
					std::string e = "Press I to open the instructions.";
					std::string f = "At end, the score will be displayed.";

					float txSize = config::screen::height / 24;

					DrawText(&tx[0], config::screen::width / 2 - MeasureText(&tx[0], txSize+20) / 2, config::screen::height * 1 / 20, txSize+20, RED);
					DrawText(&a[0], config::screen::width / 15, config::screen::height * 3 / 20, txSize, BLACK);
					DrawText(&b[0], config::screen::width / 15, config::screen::height * 6 / 20, txSize, BLACK);
					DrawText(&c[0], config::screen::width / 15, config::screen::height * 9 / 20, txSize, BLACK);
					DrawText(&d[0], config::screen::width / 15, config::screen::height * 12 / 20, txSize, BLACK);
					DrawText(&e[0], config::screen::width / 15, config::screen::height * 15 / 20, txSize, BLACK);
					DrawText(&f[0], config::screen::width / 15, config::screen::height * 18 / 20, txSize, BLACK);
				}
			}
			else {

				using namespace ResolutionWindow;

				DrawRectangleRec(pRec, pauseBG);
				DrawRectangleLinesEx(pRec, 3, BLACK);
				DrawText(&pText.tx[0], pText.pos.x, pText.pos.y, pText.size, pText.color);
				DrawText(&score.tx[0], score.pos.x, score.pos.y, score.size, score.color);
				if (backToMenuButton.selected) {
					backToMenuButton.color = LIME;
				}
				else {
					backToMenuButton.color = WHITE;
				}
				drawButton(backToMenuButton);

			}

			EndDrawing();

		}

		void deinit() {

			for (short i = 0; i < LVL_COUNT; i++) {

				UnloadTexture(BGF[i][0].TX);
				UnloadTexture(BGL[i][0].TX);
				UnloadTexture(cloud[i].TX );
				UnloadTexture(window[i].TX);
				UnloadTexture(floor[i].TX );

			}

			UnloadMusics();

		}

		void DrawTX(Texture TX) {
			DrawTexture(TX.TX, TX.hitbox.x, TX.hitbox.y, TX.color);
		}
		void DebugLines(Texture TX, Color color) {
			DrawRectangleLinesEx(TX.hitbox, 2, color);
		}

		void ShootMissile() {

			missile.visible = true;
			missile.obj.hitbox.x += GetFrameTime() * 300;

			if (CheckCollisionRecs(missile.obj.hitbox, end)) {

				missile.active = false;
				missile.visible = false;
				ResetMissile();

			}

		}

		void ResetMissile() {

			missile.obj.hitbox.x = player.obj.hitbox.x + player.obj.TX.width / 2 + missile.obj.TX.width / 2;
			missile.obj.hitbox.y = player.obj.hitbox.y + player.obj.TX.height / 2 - missile.obj.TX.height / 2;
			missile.ordY = player.ordY;

		}

		void ResetCloud(Texture &cloud) {

			int axisVar = 0;
			(GetRandomValue(0, 100) % 2 == 1) ? axisVar = GetRandomValue(0, 15) : axisVar = -GetRandomValue(0, 15);
			cloud.TX.height = config::screen::height / 13 + axisVar;
			(GetRandomValue(0, 100) % 2 == 1) ? axisVar = GetRandomValue(0, 15) : axisVar = -GetRandomValue(0, 15);
			cloud.TX.width = config::screen::width / 6 + axisVar;
			(GetRandomValue(0, 100) % 2 == 1) ? axisVar = GetRandomValue(0, 15) : axisVar = -GetRandomValue(0, 15);
			cloud.hitbox.y = config::screen::height * static_cast<int>(cloud.ordY) / 3 + cloud.TX.height / 3 + axisVar;
			cloud.hitbox.x = config::screen::width - cloud.TX.width / 2;

		}

		void ResetBackground(Texture& BG) {

			BG.hitbox.x = end.x - 10;
			//BG.hitbox.x = BG.TX.width - 10 + BG.hitbox.x;

		}

		void LoadMusics() {
			
			buttonBeep = LoadSound("res/assets/Sounds/ButtonPip.ogg");
			shot = LoadSound("res/assets/Sounds/missile.wav");
			BGM = LoadMusicStream("res/assets/Sounds/gameplay.mp3");
			PlayMusicStream(BGM);

		}

		void UnloadMusics() {
			if (IsMusicPlaying(BGM)) {
				StopMusicStream(BGM);
			}
			if (IsSoundPlaying(shot)) {
				StopSound(shot);
			}
			UnloadSound(shot);
			UnloadMusicStream(BGM);
		}

		void CreateEnemy(GameObject& enemy) {
			
			std::string typ;

			enemy.type = static_cast<EnemyType>(GetRandomValue(0, (HowManyEnemyTypes - 1)));
			enemy.ordY = static_cast<OrderY>(GetRandomValue(0, (LVL_COUNT - 1)));
			enemy.obj = enemies[static_cast<int>(enemy.type)];
			
			switch (enemy.type) {

			case endless::game::EnemyType::WEAK:
				typ = " WEAK\n";
				enemy.obj.hitbox.y = config::screen::height * (static_cast<int>(enemy.ordY) + 1) / 3 - enemy.obj.TX.height * 13 / 10;
				break;
			case endless::game::EnemyType::NORMAL:
				enemy.obj.hitbox.y = config::screen::height * (static_cast<int>(enemy.ordY) + 1) / 3 - enemy.obj.TX.height * 13 / 10;
				typ = " NORMAL\n";
				break;
			case endless::game::EnemyType::GIANT:
				typ = " GIANT\n";
				enemy.obj.hitbox.y = config::screen::height * (static_cast<int>(enemy.ordY) / 3) - 10;
				break;
			}

			enemy.obj.hitbox = { enemy.obj.hitbox.x, enemy.obj.hitbox.y, static_cast<float>(enemy.obj.TX.width), static_cast<float>(enemy.obj.TX.height) };

			enemy.destroyable = enemy.type != EnemyType::WEAK;
			enemy.jumping = false;
			enemy.visible = true;
			enemy.active = true;

			

			std::cout << "Fabrica de Enemigo:" << typ;

		}

		void CreateResolutionWindow() {

			using namespace ResolutionWindow;

			#pragma region RESOLUTION AREA

			pRec.height = config::screen::height / 2;
			pRec.width = config::screen::width / 2;
			pRec.x = config::screen::width / 2 - pRec.width / 2;
			pRec.y = config::screen::height / 2 - pRec.height / 2;

			pText.tx = "GAME OVER";
			pText.size = config::screen::height / 11;
			pText.pos.x = config::screen::width / 2 - MeasureText(&pText.tx[0], pText.size) / 2;
			pText.pos.y = pRec.y + pText.size * 5 / 7;
			pText.color = BLACK;

			#pragma region SCORE

			std::string asd = std::to_string(enemyKillCount);
			score.tx = "Score ";
			score.tx.append(asd);
			score.size = config::screen::height * 4 / 39;
			score.pos.x = pRec.x + pRec.width / 2 - (MeasureText(&score.tx[0], score.size) / 2);
			score.pos.y = pText.pos.y + pText.size * 3 / 2;
			score.color = BLACK;

#pragma endregion

			#pragma region MAIN MENU

			mainMenu.txt.tx = "MAIN MENU";
			mainMenu.id = menuOptions::MENU;
			mainMenu.txt.size = config::screen::height * 2 / 39;
			mainMenu.square.width = MeasureText(&mainMenu.txt.tx[0], mainMenu.txt.size) * 3 / 2;
			mainMenu.square.height = mainMenu.txt.size * 2;
			mainMenu.square.x = pRec.x + pRec.width / 2 - mainMenu.square.width / 2;
			mainMenu.square.y = pText.pos.y + pText.size * 3;
			mainMenu.color = pauseButtonsColor;
			mainMenu.txt.pos.x = mainMenu.square.x + mainMenu.square.width / 2 - MeasureText(&mainMenu.txt.tx[0], mainMenu.txt.size) / 2;
			mainMenu.txt.pos.y = mainMenu.square.y + mainMenu.square.height / 2 - mainMenu.txt.size / 2;
			mainMenu.txt.color = BLACK;

			

			backToMenuButton = mainMenu;

#pragma	endregion

#pragma endregion

			resolCreation = false;
		}

		void BackToMenuCheck() {

			using namespace ResolutionWindow;

			Vector2 mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, backToMenuButton.square)) {

				if (!backToMenuButton.selected) {
					//PlaySound(buttonS);
				}
				backToMenuButton.selected = true;

				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON)) {

					config::scenes::next_scene = config::scenes::Scene::MENU;

				}

			}
			else {

				backToMenuButton.selected = false;

			}


		}

		void PauseMenuCheck() {
			
			using namespace pauseMenu;

			Vector2 mousePoint = GetMousePosition();

			for (int i = 0; i < howManyPauseButtons; i++) {

				if (CheckCollisionPointRec(mousePoint, pauseButtons[i].square)) {

					if (!pauseButtons[i].selected) {
						PlaySound(buttonBeep);
					}
					pauseButtons[i].selected = true;

					if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON)) {

						if (i == 0) {
							config::scenes::next_scene = config::scenes::Scene::GAME;
						}
						else {
							config::scenes::next_scene = config::scenes::Scene::MENU;
						}
						pause = false;
					}

				}
				else {

					pauseButtons[i].selected = false;

				}

			}
		}

	}

}