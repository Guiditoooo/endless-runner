#ifndef MENU_GAME
#define MENU_GAME

#include "raylib.h"
#include "Extern Vars/extern.h"

namespace endless {

	namespace game {

		namespace pauseMenu {

			const int howManyPauseButtons = 2;

			static Rectangle pRec;
			static Text pText;

			static Button resume;
			static Button mainMenu;

			static Button pauseButtons[howManyPauseButtons];

		}

		static const Color pauseBG = { 0, 194, 129, 255 }; //Cyann
		static const Color pauseButtonsColor = { 180, 238, 219, 255 }; //LightCyann

		namespace ResolutionWindow {

			static Rectangle pRec;
			static Text pText;

			static Text score;
			static Button mainMenu;

			static Button backToMenuButton;

		}

		const int BG_COUNT = 3;
		const int HowManyEnemies = 10;
		const int HowManyEnemyTypes = 3;
		const float JumpMax = 50.0f;

		extern Texture BGF[LVL_COUNT][BG_COUNT];
		extern Texture BGL[LVL_COUNT][BG_COUNT];
		extern Texture cloud[LVL_COUNT];
		extern Texture sun[LVL_COUNT];
		extern Texture window[LVL_COUNT];
		extern Texture floor[LVL_COUNT];
		extern Texture enemies[HowManyEnemyTypes];


		extern Music BGM;
		
		extern Sound shot;
		extern Sound p_death;
		extern Sound e_death;

		extern GameObject player;
		extern GameObject missile;
		extern GameObject enemy[HowManyEnemies];

		extern Rectangle begin;
		extern Rectangle end;

		extern int cloudVel[LVL_COUNT];
		extern int BGFVel[LVL_COUNT];
		extern int BGLVel[LVL_COUNT];
		extern int enemyVel[HowManyEnemies];

		extern bool pause;
		extern bool pauseMusic;
		extern bool pauseSounds;

		extern int enemy_count;
		extern float frames_expend;
		extern int time_in_seconds;

		void init();
		void CreateResolutionWindow();
		void update();
		void draw();
		void deinit();

		void DrawTX(Texture TX);
		void DebugLines(Texture TX, Color color);
		void ShootMissile();
		void ResetMissile();
		void ResetCloud(Texture& cloud);
		void ResetBackground(Texture& BG);
		void LoadMusics();
		void UnloadMusics();
		void CreateEnemy(GameObject& enemy);
		void BackToMenuCheck();
		void PauseMenuCheck();

	}

}
#endif